# Shut The Box Simulation

Shut The Box is a simple gambling game about knocking down pegs based upon the roll of two six sided dice. See the [Wikipedia Article](https://en.wikipedia.org/wiki/Shut_the_Box) for more details.

This repository contains a basic simulation of multiple game strategies to see which has the best success rate.

# Result

| Method | Win Percentage | Average Score |
| --- | --- | ---|
| Random | 1.8% | 20.4|
| Close Highest | 6.0% | 13.0 |
| Close Most | 1.1% | 24.0 |

# Methods

## Random

Choose a random valid combination of pegs and close them

## Close Highest

Close the combination of pegs that has the highest range between largest and smallest unless the number rolled exists as a peg. e.g.

Given a roll of `{3, 5} = 8` close first close `{8}` then close `{7, 1}` then close `{2, 6}`.

This strategy always rolls two dice.

## Close most

Always close as many pegs as possible on each turn