
use rand::{
    thread_rng,
    Rng,
};

pub fn roll_d6() -> u8 {
    let mut rng = thread_rng();
    rng.gen_range(1, 7)
}

pub fn roll_2d6() -> u8 {
    roll_d6() + roll_d6()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashSet;
    use itertools::Itertools;

    #[test]
    fn test_roll_d6() {
        let rolls: HashSet<u8> = (0..1000).map(|_| roll_d6()).collect();
        assert_eq!(rolls.len(), 6);

        let (min, max) = rolls.iter().minmax().into_option().unwrap();
        assert_eq!(min, &1);
        assert_eq!(max, &6);
    }
}