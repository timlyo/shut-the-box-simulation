mod the_box;
mod dice;
mod strategy;
mod util;
mod stats;

use crate::strategy::Strategy;

const NUM_RUNS: usize = 5000;

fn main() {
    println!("Running");
    let random = repeat_game(strategy::Random);
    let close_highest = repeat_game(strategy::CloseHighest);
    let close_most = repeat_game(strategy::CloseMost);

    println!("Random: {:?}", stats::calc_stats(random));
    println!("Close Highest: {:?}", stats::calc_stats(close_highest));
    println!("Close Most: {:?}", stats::calc_stats(close_most));
}

pub struct Score(u8);
impl Score{
    pub fn is_win(&self) -> bool{
        self.0 == 0
    }
}

fn repeat_game<S: Strategy>(strat: S) -> Vec<Score> {
    (0..NUM_RUNS).map(|_| play_game(&strat)).collect()
}

fn play_game<S: Strategy>(strat: &S) -> Score{
    let mut the_box = the_box::Box::new(9);

    while the_box.is_shut() == false {
//        println!("Pegs: {:?}", the_box.get_sorted_vec_of_open_pegs());
        let roll = dice::roll_2d6();
//        println!("Rolled: {}", roll);

        if strat.play(&mut the_box, roll) == false {
//            println!("Couldn't close anything");
            break;
        }
    }

    let score = the_box.get_sorted_vec_of_open_pegs().iter().sum();
    Score(score)
}