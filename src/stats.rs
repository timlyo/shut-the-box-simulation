use crate::Score;

#[derive(Debug)]
pub struct Stats {
    wins: u32,
    losses: u32,
    percent: f32,
    average: f32
}


pub fn calc_stats(games: Vec<Score>) -> Stats {
    let mut wins = 0;
    let mut losses = 0;
    let mut total = 0;

    for game in games {
        total += game.0 as u64;
        if game.is_win() {
            wins += 1
        } else {
            losses += 1
        }
    }

    let game_count = wins + losses;

    let percent = (wins as f32 / game_count as f32) * 100.0;
    let average = total as f32 / game_count as f32;

    Stats {
        wins,
        losses,
        percent,
        average,
    }
}