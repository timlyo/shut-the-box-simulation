use crate::strategy::Strategy;
use crate::the_box::Box;
use std::collections::HashSet;
use crate::util;
use itertools::Itertools;
use std::ops::Sub;

pub struct CloseHighest;

fn get_vec_range<A: Sub<Output=A> + PartialOrd + Copy>(vec: &Vec<A>) -> A {
    let (min, max) = vec
        .into_iter().minmax()
        .into_option().expect("Couldn't get vector minmax");
    *max - *min
}

impl CloseHighest {
    pub fn get_highest_equal_to_roll<'a>(set: &HashSet<u8>, roll: u8) -> Option<Vec<u8>> {
        if set.is_empty() {
            return None;
        }

        if set.contains(&roll) {
            Some(vec![roll])
        } else {
            let valid_combos = util::subset_with_sum_equal_to(&set, roll);
            if valid_combos.is_empty(){
                return None;
            }

            let mut ranked = valid_combos.into_iter()
                .map(|combo| (get_vec_range(&combo), combo))
                .collect::<Vec<(u8, Vec<u8>)>>();
            ranked.sort_by_key(|c| c.0);

            let (range, combo) = ranked.last().expect("Couldn't get last from vec");
            Some(combo.clone())
        }
    }
}

impl Strategy for CloseHighest {
    fn play(&self, the_box: &mut Box, roll: u8) -> bool {
        let set = the_box.get_open_under_roll(roll);

        if let Some(highest_combo) = CloseHighest::get_highest_equal_to_roll(&set, roll) {
//            println!("Closing {:?}", highest_combo);
            for peg in highest_combo {
                the_box.close(peg).unwrap();
            }
            true
        }else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::assert_vec_has_same_elements;
    use crate::play_game;

    fn make_set() -> HashSet<u8> {
        let mut set = HashSet::new();
        set.insert(1u8);
        set.insert(2u8);
        set.insert(3u8);

        set
    }

    #[test]
    fn test_close_highest() {
        play_game(&CloseHighest);
    }

    #[test]
    fn get_highest_when_equal() {
        let set = make_set();

        assert_eq!(CloseHighest::get_highest_equal_to_roll(&set, 1), Some(vec![1]));
        assert_eq!(CloseHighest::get_highest_equal_to_roll(&set, 2), Some(vec![2]));
        assert_eq!(CloseHighest::get_highest_equal_to_roll(&set, 3), Some(vec![3]));
    }

    #[test]
    fn get_highest_with_combo() {
        let set = make_set();

        assert_vec_has_same_elements(
            CloseHighest::get_highest_equal_to_roll(&set, 4).unwrap(), vec![1, 3]);
        assert_vec_has_same_elements(
            CloseHighest::get_highest_equal_to_roll(&set, 5).unwrap(), vec![2, 3]);
        assert_vec_has_same_elements(
            CloseHighest::get_highest_equal_to_roll(&set, 6).unwrap(), vec![1, 2, 3]);
    }
}
