use crate::strategy::Strategy;
use crate::the_box::Box;
use crate::util;
use rand::seq::SliceRandom;
use rand::thread_rng;

pub struct CloseMost;

impl Strategy for CloseMost {
    fn play(&self, the_box: &mut Box, roll: u8) -> bool {
        let set = the_box.get_open_under_roll(roll);

        let mut valid_combos = util::subset_with_sum_equal_to(&set, roll);
        valid_combos.sort_by_key(|c| c.len());

        if valid_combos.is_empty(){
            return false
        }

        let largest_size = valid_combos.last().expect("Couldn't get last")
            .len();

        let choice = valid_combos.into_iter()
            .filter(|c| c.len() == largest_size)
            .collect::<Vec<Vec<u8>>>()
            .choose(&mut thread_rng()).expect("Couldn't choose and element").clone();

        the_box.close_multiple(choice).is_ok()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::play_game;

    #[test]
    fn test_close_most() {
        play_game(&CloseMost);
    }
}