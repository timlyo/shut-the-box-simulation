mod close_highest;
mod random;
mod close_most;

use crate::the_box::Box;

pub use crate::strategy::random::Random;
pub use crate::strategy::close_highest::CloseHighest;
pub use crate::strategy::close_most::CloseMost;


pub trait Strategy {
    /// Make a single play, return true if a turn was taken
    fn play(&self, the_box: &mut Box, roll: u8) -> bool;
}

