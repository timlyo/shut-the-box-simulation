use crate::strategy::Strategy;
use crate::the_box::Box;
use rand::thread_rng;
use crate::util;
use std::collections::HashSet;
use rand::seq::SliceRandom;

pub struct Random;

impl Strategy for Random {
    fn play(&self, the_box: &mut Box, roll: u8) -> bool {
        let set = the_box.get_open_under_roll(roll);

        let valid = util::subset_with_sum_equal_to(&set, roll);

        let choice = match valid.choose(&mut thread_rng()) {
            Some(c) => c.clone(),
            None => return false
        };

//        println!("Closing {:?}", choice);

        the_box.close_multiple(choice).is_ok()
    }
}