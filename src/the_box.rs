use std::collections::HashSet;

pub struct Box {
    // Vector for still standing pegs
    pegs: Vec<State>
}

#[derive(Debug)]
pub enum Error {
    PegAlreadyClosed(u8),
    IndexError(usize),
}

#[derive(Debug, PartialOrd, PartialEq)]
pub enum State {
    Open,
    Closed,
}


impl Box {
    pub fn new(size: usize) -> Box {
        Box {
            pegs: (0..size).map(|_| State::Open).collect()
        }
    }

    pub fn size(&self) -> usize {
        self.pegs.len()
    }

    pub fn close_multiple(&mut self, numbers: Vec<u8>) -> Result<(), Error>{
        // Check pegs aren't already closed
        for n in &numbers{
            let index = (n-1) as usize;
            if let Some(peg) = self.pegs.get(index){
                if *peg == State::Closed{
                    return Err(Error::PegAlreadyClosed(n.clone()))
                }
            }
        }

        for n in numbers{
            // Should be ok after check above
            self.close(n).expect("Couldn't close peg");
        }

        Ok(())
    }

    pub fn close(&mut self, number: u8) -> Result<(), Error> {
        let index = number as usize -1;

        match self.pegs.get_mut(index) {
            Some(peg) => {
                if *peg == State::Closed {
                    return Err(Error::PegAlreadyClosed(number));
                }
                *peg = State::Closed
            }
            None => return Err(Error::IndexError(index))
        };

        Ok(())
    }

    /// Returns true if all pegs are closed
    pub fn is_shut(&self) -> bool {
        let num_open = self.pegs.iter()
            .filter(|peg| *peg == &State::Open)
            .count();

        num_open == 0
    }

    pub fn pegs(&self) -> &Vec<State> {
        &self.pegs
    }

    pub fn get_set_of_open_pegs(&self) -> HashSet<u8> {
        self.pegs.iter()
            .enumerate()
            .filter(|(_index, state)| *state == &State::Open)
            .map(|(index, _state)| (index as u8) + 1)
            .collect()
    }

    /// Get Set of all open pegs that are under a certain value
    pub fn get_open_under_roll(&self, roll: u8) -> HashSet<u8>{
        self.get_set_of_open_pegs().into_iter()
            .filter(|peg| *peg <= roll).collect()
    }

    pub fn get_sorted_vec_of_open_pegs(&self) -> Vec<u8> {
        let mut pegs: Vec<u8> = self.pegs.iter()
            .enumerate()
            .filter(|(_index, state)| *state == &State::Open)
            .map(|(index, _state)| (index as u8) + 1)
            .collect();

        pegs.sort();
        pegs
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter::FromIterator;

    #[test]
    fn box_size_correct() {
        let the_box = Box::new(9);
        assert_eq!(the_box.size(), 9)
    }

    #[test]
    fn close_peg() {
        let mut the_box = Box::new(9);
        assert!(the_box.close(4).is_ok());
        assert_eq!(the_box.pegs()[3], State::Closed);
    }

    #[test]
    fn close_peg_that_doesnt_exist() {
        let mut the_box = Box::new(9);
        assert!(the_box.close(10).is_err());
    }

    #[test]
    fn is_closed() {
        let mut the_box = Box::new(3);
        assert!(the_box.close(1).is_ok());
        assert!(the_box.close(2).is_ok());
        assert!(the_box.close(3).is_ok());

        assert!(the_box.is_shut());
    }

    #[test]
    fn get_set_of_open_pegs() {
        let the_box = Box::new(3);
        assert_eq!(
            HashSet::from_iter(vec![1, 2, 3].iter().cloned()),
            the_box.get_set_of_open_pegs()
        )
    }

    #[test]
    fn test_get_sorted_vec_of_open_pegs() {
        let the_box = Box::new(3);

        assert_eq!(
            the_box.get_sorted_vec_of_open_pegs(),
            vec![1, 2, 3]
        )
    }
    #[test]
    fn test_get_sorted_vec_of_open_pegs_after_close() {
        let mut the_box = Box::new(3);
        the_box.close(2);

        assert_eq!(
            the_box.get_sorted_vec_of_open_pegs(),
            vec![1, 3]
        )
    }
}

