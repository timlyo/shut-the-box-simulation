use std::collections::HashSet;
use itertools::Itertools;
use std::fmt::Debug;

pub fn subset_with_sum_equal_to(set: &HashSet<u8>, roll: u8) -> Vec<Vec<u8>> {
    combinations(set).into_iter()
        .filter(|c| c.iter().sum::<u8>() == roll)
        .collect()
}

pub fn combinations(set: &HashSet<u8>) -> Vec<Vec<u8>> {
    let combo_sizes = 1..(set.len() + 1);

    combo_sizes
        .flat_map(
            |set_size| set.iter().cloned().combinations(set_size).collect::<Vec<Vec<u8>>>()
        ).collect()
}

#[cfg(test)]
pub fn assert_vec_has_same_elements<A: PartialOrd + Debug>(l: Vec<A>, r:Vec<A>){
    assert_eq!(l.len(), r.len(), "Vectors are not the same length");

    for element in l{
        assert!(r.contains(&element), "{:?} not found in {:?}", element, r);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter::FromIterator;

    fn make_set() -> HashSet<u8> {
        HashSet::from_iter(1..4)
    }

    #[test]
    fn test_combinations() {
        let set = make_set();

        let mut combos = combinations(&set).iter_mut()
            .map(|v| {
                v.sort();
                v.clone()
            })
            .collect::<Vec<Vec<u8>>>();

        combos.sort();

        assert_eq!(
            combos,
            vec![
                vec![1],
                vec![1, 2],
                vec![1, 2, 3],
                vec![1, 3],
                vec![2],
                vec![2, 3],
                vec![3],
            ]
        )
    }

    #[test]
    fn test_subset_with_sum_equal_to() {
        let set = make_set();

        for x in 0..7 {
            for c in subset_with_sum_equal_to(&set, x) {
                assert_eq!(c.into_iter().sum::<u8>(), x);
            }
        }
    }

    #[test]
    fn test_subset_with_sum_equal_to_multiple_result() {
        let set = make_set();
        let result: Vec<Vec<u8>> = subset_with_sum_equal_to(&set, 4)
            .iter_mut().map(|c| {c.sort(); c.clone()}).collect();

        assert_eq!(result, vec![vec![1, 3]]);
    }
}